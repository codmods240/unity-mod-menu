﻿using UnityEngine;

public class ModMenuScript : MonoBehaviour {

    public bool hack1;
    public bool hack2;
    public bool hack3;
    public string string1;
    public string string2;
    public string string3;
    public bool ShowHide = false;

    void OnGUI() {

        if (GUI.Button(new Rect(20, 20, 130f, 30f), "SHOW/HIDE"))
        {
            ShowHide = !ShowHide;
        }

        if (ShowHide)
        {
            GUI.Box(new Rect(20, 50, 180, 150), "iAndroHacker's mod menu");
            if (GUI.Button(new Rect(25, 80, 170f, 30f), string1))
            {
                hack1 = !hack1;
            }
            if (GUI.Button(new Rect(25, 115, 170f, 30f), string2))
            {
                hack2 = !hack2;
            }
            if (GUI.Button(new Rect(25, 150, 170f, 30f), string3))
            {
                hack3 = !hack3;
            }
        }

        if (hack1)
        {
            string1 = "Unlimited armor <color=green>ON</color>";
            hack1 = false;
        }
        else
        {
            string1 = "Unlimited armor <color=red>OFF</color>";
            hack1 = true;
        }

        if (hack2)
        {
            string2 = "Unlimited team health <color=green>ON</color>";
            hack2 = false;
        }
        else
        {
            string2 = "Unlimited team health <color=red>OFF</color>";
            hack2 = true;
        }

        if (hack3)
        {
            string3 = "Unlimited coins <color=green>ON</color>";
            hack3 = false;
        }
        else
        {
            string3 = "Unlimited coins <color=red>OFF</color>";
            hack3 = true;
        }
    }
}
