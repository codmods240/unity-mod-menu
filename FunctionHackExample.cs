//NOTE: This is an example code of hacking shared function of player and enemy.

public int attack
{
	get
	{
		if (ModMenuScript.hack1)
		{
			if (this.Data.Owner == MonsterData.OwnerType.ENEMY && !FieldController.isMultiplayer)
			{
				num4 = (float)this.Data.attack / DifficultyController.GetEnemyStatsMultiplier();
			}
		}
		else
		{
				num4 = (float)this.Data.attack * DifficultyController.GetEnemyStatsMultiplier();
		}
		if (ModMenuScript.hack2 && this.Data.Owner == MonsterData.OwnerType.PLAYER)
		{
				return 99999999;
		}
	}
}
public int defence
{
	get
	{
		if (ModMenuScript.hack2)
		{
			if (this.Data.Owner == MonsterData.OwnerType.ENEMY && !FieldController.isMultiplayer)
			{
				num4 = (float)this.Data.defence / DifficultyController.GetEnemyStatsMultiplier();
			}
			else
			{
				num4 = (float)this.Data.defence * DifficultyController.GetEnemyStatsMultiplier();
			}
		}
		if (ModMenuScript.hack4 && this.Data.Owner == MonsterData.OwnerType.PLAYER)
		{
			return 99999999;
		}
	}
}